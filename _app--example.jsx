import React from 'react';
import NextApp from 'next/app';
import NextRouter from 'next/router';
import UserContext from '../components/UserContext';

export default class MyNextApp extends NextApp {
  state = { user: null };

  componentDidMount = () => {
    const user = localStorage.getItem('coolapp-user');
    if (user) this.setState({ user });
    else NextRouter.push('/signin');
  };

  signIn = (username, password) => {
    localStorage.setItem('coolapp-user', username);
    this.setState({ user: username },
      () => { NextRouter.push('/') }
    );
  };

  signOut = () => {
    localStorage.removeItem('coolapp-user');
    this.setState({ user: null });
    NextRouter.push('/signin');
  };

  render () {
    const { Component, pageProps } = this.props;

    return (
      <UserContext.Provider value={{ user: this.state.user, signIn: this.signIn, signOut: this.signOut }}>
        <Component {...pageProps} />
      </UserContext.Provider>
    );
  }
}
