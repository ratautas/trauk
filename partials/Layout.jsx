import React, { useState, useContext } from 'react';
import Head from '../partials/Head';
import Header from '../partials/Header';

import { StoreContext, fireAuth } from '../contexts/StoreContext';

export default props => {

  const { user, setUser } = useContext(StoreContext);

  fireAuth.onAuthStateChanged(userUpdate => {
    if (userUpdate && !user) setUser(userUpdate);
  });

  return (
    <>
      <Head title={props.title || ''} />
      <div className="app">
        <Header bemContext="app" />
        {props.children}
      </div>
    </>
  )
};
