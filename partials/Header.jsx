import React, { useState, useContext } from 'react';
import Nav from '../partials/Nav';
import NextRouter from 'next/router';

import { StoreContext, fireAuth } from '../contexts/StoreContext';

const Header = props => {

  const { user, setUser } = useContext(StoreContext);

  const logOut = () => {
    fireAuth.signOut().then(() => {
      setUser(null);
      NextRouter.push('/re');
    })
      .catch(e => setError(e.message));
  };

  return (
    <header className={`${props.bemContext}__header header`}>
      {user && (
        <div className="">{user.email} (<span className="" onClick={logOut}>Atsijungti</span>)</div>
      )}
      <Nav bemContext="header" />
    </header>
  )
};

export default Header;
