import React, { useState, useContext } from 'react';
import NextLink from 'next/link';
import NextRouter from 'next/router';

import { StoreContext, fireAuth } from '../contexts/StoreContext';

const links = [
  { href: '/', label: 'Home' },
  { href: '/login', label: 'Prisijungti' },
  { href: '/re', label: 'RE' },
];

const Nav = props => {
  const [error, setError] = useState(null);
  const { user, setUser } = useContext(StoreContext);

  return (
    <nav className={`${props.bemContext}__nav nav`}>
      <ul className="nav__list">
        {links.map((link, l) => {
          return (
            <li key={l} className="nav__item">
              <NextLink href={link.href}>
                <a className="nav__trigger">{link.label}</a>
              </NextLink>
            </li>
          );
        })}
      </ul>
    </nav>
  )
};

export default Nav
