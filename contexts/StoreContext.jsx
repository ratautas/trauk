// https://benmcmahen.com/using-firebase-with-react-hooks/
import React, { createContext, useState, useEffect } from 'react';
import { useCollectionData } from 'react-firebase-hooks/firestore';
import { values } from 'lodash';

import * as firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyBvZmELIZD9rVfwXg25yv6ubkR1pea4HPQ",
  authDomain: "trrauk.firebaseapp.com",
  databaseURL: "https://trrauk.firebaseio.com",
  projectId: "trrauk",
  storageBucket: "trrauk.appspot.com",
  messagingSenderId: "199178058126",
  appId: "1:199178058126:web:67ebb74e4f2ab780464cdf",
  measurementId: "G-0D06358YLZ"
};

// Initialize Firebase once
export const firebaseApp = !firebase.apps.length
  ? firebase.initializeApp(firebaseConfig)
  : firebase.app();

export const StoreContext = createContext();

export const fireStore = firebase.firestore();
export const fireAuth = firebase.auth();

export const usersRef = fireStore.collection('users');
export const productsRef = fireStore.collection('products');

export const StoreContextProvider = (props) => {

  const [user, setUser] = useState(null);

  return (
    <StoreContext.Provider value={{ user, setUser }}>
      {props.children}
    </StoreContext.Provider>
  );
}