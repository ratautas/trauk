import { createContext, useState } from 'react';

export const UserContext = createContext();

export const UserContextProvider = (props) => {

  const [errorBag, setErrorBag] = useState({ temp: 'value' });
  const [user, setUser] = useState({ temp: 'value' });

  const loginWithEmail = (email, password) => {
    authRef
      .signInWithEmailAndPassword(email, password)
      .catch(error => setErrorBag({ login: error.message }));
  };

  return (
    <UserContext.Provider value={{
      loginWithEmail,
      errorBag,
      setErrorBag,
    }}>
      {props.children}
    </UserContext.Provider>
  );
}