// https://benmcmahen.com/using-firebase-with-react-hooks/
import React, { createContext, useState, useEffect } from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useCollectionData } from 'react-firebase-hooks/firestore';
import { values } from 'lodash';


import * as firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyBvZmELIZD9rVfwXg25yv6ubkR1pea4HPQ",
  authDomain: "trrauk.firebaseapp.com",
  databaseURL: "https://trrauk.firebaseio.com",
  projectId: "trrauk",
  storageBucket: "trrauk.appspot.com",
  messagingSenderId: "199178058126",
  appId: "1:199178058126:web:67ebb74e4f2ab780464cdf",
  measurementId: "G-0D06358YLZ"
};

// Initialize Firebase once
export const firebaseApp = !firebase.apps.length
  ? firebase.initializeApp(firebaseConfig)
  : firebase.app();

export const FirebaseContext = createContext();

export const fireStore = firebase.firestore();
export const fireAuth = firebase.auth();

export const usersRef = fireStore.collection('users');
export const productsRef = fireStore.collection('products');

export const FirebaseContextProvider = (props) => {

  const [errorBag, setErrorBag] = useState({});
  
  const [userData, setUserData] = useState({});
  const [users, setUsers] = useState({});

  const [user] = useAuthState(fireAuth);

  const [products] = useCollectionData(productsRef, { idField: 'id' });

  const clearErrorBag = () => setErrorBag({});

  // const collectUserData = ref => {
  //   if (typeof userData[ref] === 'undefined' && user) {
  //     // usersRef.doc(`${user.uid}`).collection(ref).onSnapshot(snapshot => {
  //     usersRef.doc(`${user.uid}`).collection(ref).get().then(snapshot => {
  //       setUserData({
  //         ...userData,
  //         [ref]: snapshot.docs.reduce((acc, doc) => {
  //           return { ...acc, [doc.id]: doc.data() }
  //         }, {})
  //       })
  //     })
  //   }
  // };

  const loginWithEmail = (email, password) => {
    fireAuth
      .signInWithEmailAndPassword(email, password)
      .catch(error => setErrorBag({ login: error.message }));
  };

  const registerWithEmail = (email, password) => {
    fireAuth.createUserWithEmailAndPassword(email, password)
      .then(user => usersRef.doc(user.user.uid).set({ email: user.user.email }))
      .catch(error => setErrorBag({ registration: error.message }));
  };

  // const FSCreateRecipe = recipe => {
  //   recipesRef.add(recipe).then(addedRecipe => {
  //     usersRef.doc(user.uid).collection('recipes').add({ ...addedRecipe }).then(addedUserRecipe => {
  //       values(recipe.products).forEach(product => {
  //         recipesRef.doc(addedRecipe.id).collection('products').add({ ...product });
  //         // usersRef.doc(user.uid).collection('recipes').add({ ...addedRecipe }).then(addedUserRecipe => {
  //         //   recipesRef.doc(addedRecipe.id).collection('products').add({ ...product });
  //         // });
  //       });
  //     })
  //   })
  // };

  const logOut = () => {
    setErrorBag({});
    fireAuth
      .signOut()
      .catch(error => setErrorBag({ logout: error.message }));
  }

  fireAuth.onAuthStateChanged(user => {
    if (user) {
      console.log('change auth state');
      // collectUserData('recipes');
      // collectUserData('products');
      // collectUserData('listers');
    }
  });


  return (
    <FirebaseContext.Provider value={{
      clearErrorBag,
      errorBag,
      loginWithEmail,
      logOut,
      registerWithEmail,
      user,
      productsRef,
      products,
      users,
      userData,
    }}>
      {props.children}
    </FirebaseContext.Provider>
  );
}

export const withFirebaseHOC = Component => props => (
  <FirebaseContext.Consumer>
    {state => <Component {...props} firebase={state} />}
  </FirebaseContext.Consumer>
)