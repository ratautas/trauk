import React, { useState, useContext, useEffect } from 'react';

import Layout from '../partials/Layout';

import { StoreContext, fireAuth } from '../contexts/StoreContext';

const Login = props => {

  const { errorBag, setErrorBag } = useContext(StoreContext);

  const [email, setEmail] = useState('');
  const [error, setError] = useState(null);

  const handleSubmit = (e) => {
    e.preventDefault();
    fireAuth
      .sendSignInLinkToEmail(email, {
        url: `${location.origin}/verify`,
        handleCodeInApp: true,
      })
      .then(() => window.localStorage.setItem('emailForSignIn', email))
      .catch(e => setError(e.message));
  }

  return (
    <Layout title="register">
      <div className="container">
        <h2>Prisijunkite prie Kalėdinio traukimo!</h2>
        <div className="">
          <p>Prisijungimo informacija</p>
          <p>Įveskite savo paštą</p>
        </div>
        <form className="" onSubmit={handleSubmit}>
          <div className="">
            <label className=" field">
              <span className="field__placeholder">Your email</span>
              <input className="field__input" type="email" placeholder="test@mailbox.com"
                name="email" value={email} onChange={e => setEmail(e.target.value)} />
            </label>
          </div>
          <div className="">
            <button className="btn btn-outline--primary">PRISIJUNGTI</button>
          </div>
        </form>
        {error && <div>{errror}</div>}
      </div>
    </Layout >
  )
}

export default Login;