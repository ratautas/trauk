import React, { useContext } from 'react';
import NextApp from 'next/app';
import { StoreContextProvider } from '../contexts/StoreContext';

export default class App extends NextApp {

  render () {
    const { Component, pageProps } = this.props;

    return (
      <StoreContextProvider>
        <Component {...pageProps} />
      </StoreContextProvider>
    );
  }
}
