import React, { useState, useContext, useEffect } from 'react';
import NextRouter from 'next/router';

import Layout from '../partials/Layout';

import { fireAuth } from '../contexts/StoreContext';

const Verify = props => {

  const [error, setError] = useState(null);

  useEffect(() => {
    const email = localStorage.getItem('emailForSignIn')
      ? localStorage.getItem('emailForSignIn')
      : window.prompt('Please provide your email for confirmation');
    fireAuth
      .signInWithEmailLink(email, location.href)
      .then(result => {
        window.localStorage.removeItem('emailForSignIn');
        NextRouter.push('/re');
      })
      .catch(e => setError(e.message));
  }, []);

  return (
    <Layout title="register">
      <div className="container">
        <h2>Verify plz</h2>
        {error && <div>{error}</div>}
      </div>
    </Layout >
  )
}

export default Verify;